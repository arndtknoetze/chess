import React, { useState, useEffect } from 'react';
import './App.css';

function App() {

  const [board_X, setBoardX] = useState(['1', '2', '3', '4', '5', '6', '7', '8'])
  const [board_Y, setBoardY] = useState(['1', '2', '3', '4', '5', '6', '7', '8'])
  const [active_peice, setActivePeice] = useState({})
  const [highlights, setHighlights] = useState({})
  const [move_spots, setMoveSpots] = useState({})
  const [variables, setVariables] = useState({color: 'white', rounds_played: 0, taken_heroes: {'black':[], 'white': []}})
  
  let king = {
    name: 'king',
    icon: '♔',
    movements: [
      {'U': null, 'L': null},
      {'U': null, 'R': null},
      {'D': null, 'L': null},
      {'D': null, 'R': null},
      {'U': null},
      {'D': null},
      {'L': null},
      {'R': null}
    ]
  }
  let queen = {
    name: 'queen',
    icon: '♕',
    movements: [
      {'U': 1, 'L': 1},
      {'U': 1, 'R': 1},
      {'D': 1, 'L': 1},
      {'D': 1, 'R': 1},
      {'U': 1},
      {'D': 1},
      {'L': 1},
      {'R': 1}
    ]
  }
  let rook = {
    name: 'rook',
    icon: '♖',
    movements: [
      {'U': null},
      {'D': null},
      {'L': null},
      {'R': null}
    ]
  }
  let bishop = {
    name: 'bishop',
    icon: '♗',
    movements: [
      {'U': null, 'L': null},
      {'D': null, 'L': null},
      {'U': null, 'R': null},
      {'D': null, 'R': null}
    ]
  }
  let knight = {
    name: 'knight',
    icon: '♘',
    movements: [
      {'L': 1, 'U': 2},
      {'R': 1, 'U': 2},
      {'U': 1, 'R': 2},
      {'U': 1, 'L': 2},
      {'L': 1, 'D': 2},
      {'R': 1, 'D': 2},
      {'D': 1, 'R': 2},
      {'D': 1, 'L': 2},
    ]
  }
  let pawn = {
    'black': {
      name: 'pawn',
      icon: '♙',
      movements: [
        {'D': 1, 'L': 1, 'attack_only': true},
        {'D': 1, 'attack_disabled': true},
        {'D': 1, 'R': 1, 'attack_only': true},
      ],
      rules: {
        '1': {
          movements: [
            {'D': 1, 'L': 1, 'attack_only': true},
            {'D': 1, 'attack_disabled': true},
            {'D': 2, 'attack_disabled': true},
            {'D': 1, 'R': 1, 'attack_only': true},
          ]
        }
      },
      play: 1
    },
    'white': {
      name: 'pawn',
      icon: '♙',
      movements: [
        {'U': 1, 'L': 1, 'attack_only': true},
        {'U': 1, 'attack_disabled': true},
        {'U': 1, 'R': 1, 'attack_only': true},
      ],
      rules: {
        '1': {
          movements: [
            {'U': 1, 'L': 1, 'attack_only': true},
            {'U': 1, 'attack_disabled': true},
            {'U': 2, 'attack_disabled': true},
            {'U': 1, 'R': 1, 'attack_only': true},
          ]
        }
      },
      play: 1
    }
  }

  const [board_init, setBoardInit] = useState({
    '1': {
      '1': {...rook, ...{color: 'black'}},
      '2': {...knight, ...{color: 'black'}},
      '3': {...bishop, ...{color: 'black'}},
      '4': {...queen, ...{color: 'black'}},
      '5': {...king, ...{color: 'black'}},
      '6': {...bishop, ...{color: 'black'}},
      '7': {...knight, ...{color: 'black'}},
      '8': {...rook, ...{color: 'black'}}
    },
    '2': {
      '1': {...pawn['black'], ...{color: 'black'}},
      '2': {...pawn['black'], ...{color: 'black'}},
      '3': {...pawn['black'], ...{color: 'black'}},
      '4': {...pawn['black'], ...{color: 'black'}},
      '5': {...pawn['black'], ...{color: 'black'}},
      '6': {...pawn['black'], ...{color: 'black'}},
      '7': {...pawn['black'], ...{color: 'black'}},
      '8': {...pawn['black'], ...{color: 'black'}}
    },
    '3': {
      '1': { },
      '2': { },
      '3': { },
      '4': { },
      '5': { },
      '6': { },
      '7': { },
      '8': { }
    },
    '4': {
      '1': { },
      '2': { },
      '3': { },
      '4': { },
      '5': { },
      '6': { },
      '7': { },
      '8': { }
    },
    '5': {
      '1': { },
      '2': { },
      '3': { },
      '4': { },
      '5': { },
      '6': { },
      '7': { },
      '8': { }
    },
    '6': {
      '1': { },
      '2': { },
      '3': { },
      '4': { },
      '5': { },
      '6': { },
      '7': { },
      '8': { }
    },
    '7': {
      '1': {...pawn['white'], ...{color: 'white'}},
      '2': {...pawn['white'], ...{color: 'white'}},
      '3': {...pawn['white'], ...{color: 'white'}},
      '4': {...pawn['white'], ...{color: 'white'}},
      '5': {...pawn['white'], ...{color: 'white'}},
      '6': {...pawn['white'], ...{color: 'white'}},
      '7': {...pawn['white'], ...{color: 'white'}},
      '8': {...pawn['white'], ...{color: 'white'}}
    },
    '8': {
      '1': {...rook, ...{color: 'white'}},
      '2': {...knight, ...{color: 'white'}},
      '3': {...bishop, ...{color: 'white'}},
      '4': {...king, ...{color: 'white'}},
      '5': {...queen, ...{color: 'white'}},
      '6': {...bishop, ...{color: 'white'}},
      '7': {...knight, ...{color: 'white'}},
      '8': {...rook, ...{color: 'white'}}
    }
  })

  // Declare Effects
  useEffect(()=>{
    (async()=>{
      setBoardY(board_Y)
      setBoardX(board_X)
      setBoardInit(board_init)
      setActivePeice(active_peice)
      setHighlights(highlights)
      setMoveSpots(move_spots)
      setVariables(variables)
    })()
  },[])

  const stop = async (peice, X, Y, move) => {
    if(board_init[X] && board_init[X][Y] && board_init[X][Y]['name']){
      if(board_init[X][Y]['color'] === peice.color){
        return 3
      }
      if(move.attack_disabled) return 3
      return 2
    } 
    // if(move.attack_only) return 3
    return 1
  }

  const setNewVariables = async () => {
    let new_vars = variables
    new_vars.rounds_played++
    new_vars.color = (variables.color === 'white' ? 'black' : 'white')
    console.log('new_vars', new_vars)
    setVariables(new_vars)
  }

  // Declare Functions
  const peiceClick = async (peice, X, Y) => {
      console.log('----------------------------------------')
    if(active_peice.X == X && active_peice.Y == Y) {
      setActivePeice({})
      setMoveSpots({})
    }

    console.log('peice', peice)
    console.log('X', X)
    console.log('Y', Y)

    if(peice.color !== variables.color && !active_peice.peice) {
      return false
    }

    if(active_peice.peice) {

      if(move_spots[X+''+Y]){
        let new_board = board_init
        new_board[active_peice.X][active_peice.Y] = {}
        active_peice.peice.play++
        new_board[X][Y] = active_peice.peice
        setBoardInit(new_board)
        setActivePeice({})
        setMoveSpots({})
        setNewVariables()

      }
    } else {

      let movements = peice.movements;
      // if(peice.rules[peice.play] && peice.rules[peice.play].movements) {
      //   console.log('asaweawe')
      //   movements = peice.rules[peice.play].movements;
      // }

      if(movements.length > 0) {
        for (let i = 0; i < movements.length; i++) {

          let move = movements[i]
          console.log('move', move)
          
          let XX = X
          let YY = Y

          // LEFT UP / UP LEFT
          if(move.L === null && move.U === null) {
            for (let i = 1; i <= parseInt(X); i++){
              YY = parseInt(Y)-i;
              XX = parseInt(X)-i;

              let stop_Code = await stop(peice, XX, YY, move);
              if (stop_Code === 3){
                break
              } else if(stop_Code === 2) {
                move_spots[XX+''+YY] = true
                break
              } else {
                move_spots[XX+''+YY] = true
              }
            }
            setMoveSpots(move_spots)

            continue;
          } else if(move.L && move.U) {
            XX = parseInt(X)-move.U;
            YY = parseInt(Y)-move.L;

            let stop_Code = await stop(peice, XX, YY, move);
            if(stop_Code !== 3) {
              move_spots[XX+''+YY] = true
              setMoveSpots(move_spots)
            }

            continue;
          }

          // RIGHT UP / UP RIGHT
          if(move.R === null && move.U === null){
            for (let i = 1; i <= parseInt(X); i++) {
              YY = parseInt(Y)+i;
              XX = parseInt(X)-i;

              let stop_Code = await stop(peice, XX, YY, move);
              if (stop_Code === 3){
                break
              } else if(stop_Code === 2) {
                move_spots[XX+''+YY] = true
                break
              } else {
                move_spots[XX+''+YY] = true
              }
            }
            setMoveSpots(move_spots)
            continue;
          } else if(move.R && move.U) {
            XX = parseInt(X)-move.U;
            YY = parseInt(Y)+move.R;

            let stop_Code = await stop(peice, XX, YY, move);
            if(stop_Code !== 3) {
              move_spots[XX+''+YY] = true
              setMoveSpots(move_spots)
            }
            
            continue;
          }

          // LEFT DOWN / DOWN LEFT
          if(move.L === null && move.D === null) {
            for (let i = 1; i <= board_X.length-X; i++) {
                YY = parseInt(Y)-i;
                XX = parseInt(X)+i;
                let stop_Code = await stop(peice, XX, YY, move);
              if (stop_Code === 3){
                break
              } else if(stop_Code === 2) {
                move_spots[XX+''+YY] = true
                break
              } else {
                move_spots[XX+''+YY] = true
              }
            }
            setMoveSpots(move_spots)
            continue;
          } else if(move.L && move.D) {
            XX = parseInt(X)+move.D;
            YY = parseInt(Y)-move.L;
            
            let stop_Code = await stop(peice, XX, YY, move);
            if(stop_Code !== 3) {
              move_spots[XX+''+YY] = true
              setMoveSpots(move_spots)
            }

            continue;
          }

          // RIGHT DOWN / DOWN RIGHT
          if(move.R === null && move.D === null) {
            for (let i = 1; i <= board_X.length-X; i++) {
              YY = parseInt(Y)+i;
              XX = parseInt(X)+i;
              let stop_Code = await stop(peice, XX, YY, move);
              if (stop_Code === 3){
                break
              } else if(stop_Code === 2) {
                move_spots[XX+''+YY] = true
                break
              } else {
                move_spots[XX+''+YY] = true
              }
            }
            continue;
          } else if(move.R && move.D) {
            XX = parseInt(X)+move.D;
            YY = parseInt(Y)+move.R;
            
            let stop_Code = await stop(peice, XX, YY, move);
            if(stop_Code !== 3) {
              move_spots[XX+''+YY] = true
              setMoveSpots(move_spots)
            }
            
            continue;
          }

          // UP
          if(move.U === null) {
            for (let i = parseInt(X)-1; i >= 1; i--) {

                let stop_Code = await stop(peice, i, Y);
                if (stop_Code === 3){
                  break;
                } else if (stop_Code === 2) {
                  move_spots[i+''+Y] = true;
                  setMoveSpots(move_spots)
                  break;
                }

              move_spots[i+''+Y] = true;
              setMoveSpots(move_spots)

            }
            continue;
          } else if(move.U) {

            XX = parseInt(X)-move.U;

            let stop_Code = await stop(peice, XX, YY, move);
            if(stop_Code !== 3) {
              move_spots[XX+''+YY] = true
              setMoveSpots(move_spots)
            }

            continue;
          }

          // DOWN
          if(move.D === null) {
            for (let i = parseInt(X)+1; i < (board_X.length+1); i++) {

              let stop_Code = await stop(peice, i, Y);
              if (stop_Code === 3){
                break;
              } else if (stop_Code === 2) {
                move_spots[i+''+Y] = true;
                setMoveSpots(move_spots)
                break;
              }
              move_spots[i+''+Y] = true;
              setMoveSpots(move_spots)
            }
            continue;
          } else if(move.D) {

            XX = parseInt(X)+move.D;
            let stop_Code = await stop(peice, XX, YY, move);
            if(stop_Code !== 3) {
              move_spots[XX+''+YY] = true
              setMoveSpots(move_spots)
            }

            continue;
          }

          // LEFT
          if(move.L === null) {
            for (let i = parseInt(Y)-1; i >= 1; i--) {
              let stop_Code = await stop(peice, X, i);
              if (stop_Code === 3){
                break;
              } else if (stop_Code === 2) {
                move_spots[X+''+i] = true;
                setMoveSpots(move_spots)
                break;
              }
              move_spots[X+''+i] = true;
              setMoveSpots(move_spots)
            }
            continue;
          } else if(move.L) {
            YY = parseInt(Y)-move.L;
            let stop_Code = await stop(peice, XX, YY, move);

            if(stop_Code !== 3) {
              move_spots[XX+''+YY] = true
              setMoveSpots(move_spots)
            }

            continue;
          }

          // RIGHT
          if(move.R === null) {
            for (let i = parseInt(Y)+1; i < (board_Y.length+1); i++) {
              let stop_Code = await stop(peice, X, i);
              if (stop_Code === 3){
                break;
              } else if (stop_Code === 2) {
                move_spots[X+''+i] = true;
                setMoveSpots(move_spots)
                break;
              }
              move_spots[X+''+i] = true;
              setMoveSpots(move_spots)
            }
            continue;
          } else if(move.R) {
            YY = parseInt(Y)+move.R;
            
            let stop_Code = await stop(peice, XX, YY, move);
            if(stop_Code !== 3) {
              move_spots[XX+''+YY] = true
              setMoveSpots(move_spots)
            }

            continue;
          }

        }
      }
      setActivePeice({peice , X, Y})
    }
    setHighlights({})
  }

  const positionClick = async (position) => {

    if(active_peice.peice) {
      
      if(move_spots[position.X+''+position.Y]){
        let new_board = board_init
        active_peice.peice.play++
        new_board[position.X][position.Y] = active_peice.peice
        new_board[active_peice.X][active_peice.Y] = {}
  
        setBoardInit(new_board)
        setActivePeice({})
        setMoveSpots({})
        setNewVariables()
      }
    }
  }

  return (
    <div className="App">

      <div class="stats_tab">
        <h2>Stats</h2>
        <ul>
          <li>color: {variables.color}</li>
          <li>rounds_played: {variables.rounds_played}</li>
        </ul>
      </div>

      <div className="deck">
        {board_X.map((X, i)=>
          <div className="row" key={i}>
            {board_Y.map((Y, i)=>
              <div id={X+'_'+Y} className={'block '+(move_spots[X+Y] ? 'move_spot' : '')} >
                {move_spots[X+Y]}
                {board_init[X] && board_init[X][Y] && board_init[X][Y]['name'] ? 
                  <div onClick={()=> peiceClick(board_init[X][Y], X, Y)} className={board_init[X][Y]['name']+' peice '+(active_peice && active_peice.X+active_peice.Y === X+Y ? 'active_peice' : '')+' '+board_init[X][Y]['color']} >{board_init[X][Y]['icon']}</div> 
                :
                  <div onClick={()=> positionClick({X, Y})} className={'empty peice'} ></div> 
                } 
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
